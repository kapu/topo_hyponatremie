Ce topo est dans le domaine publique, en dehors des images et sons utilisées dont la licence est précisée ici:

Icons made by [Roundicons](https://www.flaticon.com/authors/roundicons) from [Flaticon](https://www.flaticon.com/) is licensed by [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/).

Icons made by [Freepik](http://www.freepik.com from [Flaticon](https://www.flaticon.com/ is licensed by [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/).

Sons poumons: IPF Lung Sound.ogg CC-BY IPFeditor on Wikipedia

Human heart beating at 61 bpm (Cc-by-3.0).ogg: licence Creative Commons Attribution 3.0 (non transposée). Source: "Benboncan" at Freesound.org http://www.freesound.org/people/Benboncan/ via wikipedia.org

Vesicular3:  Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported license by respwiki.com
